﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace gencnv2
{
    class Proces
    {
        static public void cmd(List<string> args)
        {
            try
            {
                TextWriter tw = new StreamWriter(@"c:\softbrick\log\tmp.bat");

                foreach (string arg in args)
                {
                    tw.WriteLine(arg);
                }
                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(@"c:\softbrick\log\tmp.bat");
                p.WindowStyle = ProcessWindowStyle.Hidden;
                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;

                //start bat file
                proces.Start();
                proces.WaitForExit();
            }
            catch (Exception ex)
            {
                cmd(args);
            }
        }
        static public void cmdNoWait(List<string> args)
        {
            try
            {
                TextWriter tw = new StreamWriter(@"c:\tmp2.bat");

                foreach (string arg in args)
                {
                    tw.WriteLine(arg);
                }
                tw.Close();

                //definieer tmp.bat
                ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(@"c:\tmp2.bat");
                p.WindowStyle = ProcessWindowStyle.Hidden;
                Process proces = new System.Diagnostics.Process();
                proces.StartInfo = p;

                //start bat file
                proces.Start();
                //proces.WaitForExit();
            }
            catch (Exception ex)
            {
                cmd(args);
            }
        }
    }
}
