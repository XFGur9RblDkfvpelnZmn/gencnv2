echo import mutaties contracturen
gencnv2 wochsoll ..\vst\import\Parttimeperc.csv ..\vst\import\Wochsoll.imp -v5 -d3 -s1 -hh
dbimport ..\vst\import\Wochsoll.imp -d~

echo import mutaties afdkod
gencnv2 afdkod ..\vst\import\Afdeling.csv ..\vst\import\Afdkod.imp -v4 -d3 -s1
dbimport ..\vst\import\Afdkod.imp -d~

echo import mutaties wplekkod
gencnv2 wplekkod ..\vst\import\subafdeling.csv ..\vst\import\Wplekkod.imp -v5 -d3 -s1
dbimport ..\vst\import\Wplekkod.imp -d~

echo import mutaties deeltijd
gencnv2 deeltijd ..\vst\import\Parttimeperc.csv ..\vst\import\Deeltijd.imp -v4 -d3 -s1
dbimport ..\vst\import\Deeltijd.imp -d~

echo import mutaties kpu
gencnv2 price ..\vst\import\Kostprijsbrutouurloon.csv ..\vst\import\Price.imp -v4 -d3 -s1
dbimport ..\vst\import\Price.imp -d~