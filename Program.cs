﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Win32;

namespace gencnv2
{
    class Program
    {
        public static string tabel = "personeel";
        public static string timeFormat = "";
        public static int kolomSalnum = 1, kolomDatum = 3, kolomValue = 4;
        public static int aantalTekens = 0;
        public static string veld = "";
        /// <summary>
        /// gebruik:
        /// gencnv2 type bron doel (-ttabel) (-skolomSalnum) (-dkolomDatum) (-vkolomValue) (-hh) (-f000000)
        /// </summary> 
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            #region lees args

            Program.veld = args[0];
            foreach (string arg in args)
            {
                if (arg.Substring(0, 2) == "-t")
                {
                    tabel = arg.Substring(2);
                }
                else if (arg.Substring(0, 2) == "-s")
                {
                    kolomSalnum = Convert.ToInt32(arg.Substring(2));
                }
                else if (arg.Substring(0, 2) == "-d")
                {
                    kolomDatum = Convert.ToInt32(arg.Substring(2));
                }
                else if (arg.Substring(0, 2) == "-v")
                {
                    kolomValue = Convert.ToInt32(arg.Substring(2));
                }
                else if (arg.Substring(0, 3) == "-hh")
                {
                    timeFormat = "hh";
                }
                else if (arg.Substring(0, 2) == "-f")
                {
                    aantalTekens = arg.Length-2;
                }
            }

            #endregion

            if (tabel == "personeel")
            {
                RegelPers.leesRegels(args[1]);
                RegelPers.maakDbImport(args[0], args[2]);
            }
            else if (tabel == "normtab")
            {
                Normtab.leesRegels(args[1]);
                Normtab.maakDbImport(args[2]);
            }
            else if (tabel == "wksplit")
            {
                Wksplit.leesRegels(args[1]);
                string tmp = string.Empty;
                try
                {
                    tmp = args[2];
                }
                catch
                {
                }
                Wksplit.maakDbImport(@"..\log\Wksplit.tmp",tmp);
            }
            else
            {
                List<string> cmds = new List<string>();
                cmds.Add("echo fout in argumenten, bekijk de file gencnv2Info.txt voor meer info");
                cmds.Add("pause");
                Proces.cmd(cmds);
            }
        }
        
    }
    /// <summary>
    /// Import personeel TI
    /// </summary>
    class RegelPers
    {
        public static List<RegelPers> lijst = new List<RegelPers>();
        string salnum;
        DateTime datum;
        string value;

        public RegelPers(string salnum,DateTime datum, string value)
        {
            this.salnum = salnum;
            this.datum = datum;
            this.value = value;
        }
        public static void leesRegels(string bron)
        {
            string[] regels = Lees.Regels(bron);
            foreach (string regel in regels)
            {
                try
                {
                    string[] data = regel.Split(';');
                    string salnum = data[Program.kolomSalnum-1];
                    string[] tmpDatum = data[Program.kolomDatum-1].Split('-');
                    DateTime datum = new DateTime(Convert.ToInt32(tmpDatum[2]), Convert.ToInt32(tmpDatum[1]), Convert.ToInt32(tmpDatum[0]));
                    string value = "";
                    if (Program.timeFormat.Length > 0)
                    {
                        value = (Convert.ToDecimal(data[Program.kolomValue - 1]) * 60).ToString("0000");
                    }
                    else
                    {
                        value = data[Program.kolomValue - 1];
                    }
                     value = value.Replace("\r",String.Empty).Replace("\t",String.Empty);
                    if (Program.aantalTekens > 0)
                    {
                        while (value.Length > Program.aantalTekens)
                        {
                            value = value.Substring(1);
                        }
                        while (value.Length < Program.aantalTekens)
                        {
                            value = "0" + value;
                        }
                    }
                    if (Program.veld == "price")
                    {
                        int plaats = value.IndexOf(',');
                        value = value.Remove(plaats, 1);
                    }
                    lijst.Add(new RegelPers(salnum, datum, value));
                }
                catch (Exception ex)
                {
                    string s = ex.Message;
                }
            }
        }
       
        public static void maakDbImport(string type, string doel)
        {
            string dbImport = genereerDbImportRecords(type);
            // create a writer and open the file
            TextWriter tw = new StreamWriter(doel);

            // write a line of text to the file
            tw.WriteLine(dbImport);

            // close the stream
            tw.Close();
        }
        static string genereerDbImportRecords(string type)
        {
            string dbImport = "";
            foreach (RegelPers regel in lijst)
            {
                if (type == "pers_grp")
                {
                    List<string> cmds = new List<string>();
                    cmds.Add("@ECHO OFF");
                    cmds.Add(@"cd c:\softbrick\bin\");
                    cmds.Add(@"echo " + regel.salnum + ";" + regel.value + @" > ..\log\pers_grp.tmp");
                    cmds.Add(@"gencnv ..\vst\import\pers_grp.def ..\log\pers_grp.tmp -d~  > ..\log\pers_grp.imp");
                    Proces.cmd(cmds);
                    string[] tmp = File.ReadAllLines(@"c:\softbrick\log\pers_grp.imp");
                    foreach (string str in tmp)
                    {
                        dbImport += "mut_date=" + regel.datum.ToString("yyyyMMdd") + "~" + str + "\n";
                    }
                }
                else
                {
                    dbImport += "mut_date=" + regel.datum.ToString("yyyyMMdd") + "~salix~salnum=" + regel.salnum + "~" + type + "~" + regel.value + '\n';
                }
                try
                {
                    int nom = Convert.ToInt32(regel.value);
                    #region weekoverwerk bij weeknominaal
                    string value = null;
                    if ((type == "wochsoll") && (nom < 2280))
                    {
                        if (nom < 960)
                        {
                            // PT
                            value = "004";
                        }
                        //DT
                        else if ((nom >= 960) && (nom < 1020))
                        {
                            value = "100";
                        }
                        else if ((nom >= 1020) && (nom < 1080))
                        {
                            value = "101";
                        }
                        else if ((nom >= 1080) && (nom < 1140))
                        {
                            value = "102";
                        }
                        else if ((nom >= 1140) && (nom < 1200))
                        {
                            value = "103";
                        }
                        else if ((nom >= 1200) && (nom < 1260))
                        {
                            value = "104";
                        }
                        else if ((nom >= 1260) && (nom < 1320))
                        {
                            value = "105";
                        }
                        else if ((nom >= 1320) && (nom < 1380))
                        {
                            value = "106";
                        }
                        else if ((nom >= 1380) && (nom < 1440))
                        {
                            value = "107";
                        }
                        else if ((nom >= 1440) && (nom < 1500))
                        {
                            value = "108";
                        }
                        else if ((nom >= 1500) && (nom < 1560))
                        {
                            value = "109";
                        }
                        else if ((nom >= 1560) && (nom < 1620))
                        {
                            value = "110";
                        }
                        else if ((nom >= 1620) && (nom < 1680))
                        {
                            value = "111";
                        }
                        else if ((nom >= 1680) && (nom < 1740))
                        {
                            value = "112";
                        }
                        else if ((nom >= 1740) && (nom < 1800))
                        {
                            value = "113";
                        }
                        else if ((nom >= 1800) && (nom < 1860))
                        {
                            value = "114";
                        }
                        else if ((nom >= 1860) && (nom < 1920))
                        {
                            value = "115";
                        }
                        else if ((nom >= 1920) && (nom < 1980))
                        {
                            value = "116";
                        }
                        else if ((nom >= 1980) && (nom < 2040))
                        {
                            value = "117";
                        }
                        else if ((nom >= 2040) && (nom < 2100))
                        {
                            value = "118";
                        }
                        else if ((nom >= 2100) && (nom < 2160))
                        {
                            value = "119";
                        }
                        else if ((nom >= 2160) && (nom < 2220))
                        {
                            value = "120";
                        }
                        else if ((nom >= 2220) && (nom < 2280))
                        {
                            value = "121";
                        }

                        dbImport += "mut_date=" + regel.datum.ToString("yyyyMMdd") + "~salix~salnum=" + regel.salnum + "~wmaxpar~" + value + '\n';
                    #endregion
                    }
                }
                catch (Exception ex)
                {
                    string a =ex.Message;
                }
                try
                {
                    #region weekoverwerk bij pers grp
                    int pers = Convert.ToInt32(regel.value);
                    if (type == "pers_grp")
                    {
                        string value = null;
                        if (pers == 1)
                        {
                            value = "006";
                        }
                        else if (pers == 2)
                        {
                            value = "005";
                        }
                        else if ((pers == 3) || (pers == 6) || (pers == 11) || (pers == 13) || (pers == 15))
                        {
                            value = "001";
                        }
                        else if ((pers == 5)||(pers==8)||(pers==17))
                        {
                            value = "008";
                        }
                        else if ((pers == 9)||(pers==10))
                        {
                            value = "005";
                        }
                        if(value!=null)
                        dbImport += "mut_date=" + regel.datum.ToString("yyyyMMdd") + "~salix~salnum=" + regel.salnum + "~wmaxpar~" + value + '\n';
                    #endregion
                    }
                }
                catch (Exception ex)
                {
                }
            }
            return dbImport;
        }
    }
    
    /// <summary>
    /// import normtabellen
    /// </summary>
    class Normtab
    {
        public static List<Normtab> lijst = new List<Normtab>();
        string afdkod,regelNr,omzet,uren,manuurPrest, loonkost,loonkostPerOmzet, eenheden, omzetPerEenheid,omschrijving;
        public Normtab(string afdkod, string regelNr, string omzet, string uren, string manuurPrest, string loonkost, string loonkostPerOmzet, string eenheden, string omzetPerEenheid, string omschrijving)
        {
            this.afdkod = afdkod;
            this.regelNr = regelNr;
            this.omzet = omzet;
            this.uren = uren;
            this.manuurPrest = manuurPrest;
            this.loonkost = loonkost;
            this.loonkostPerOmzet = loonkostPerOmzet;
            this.eenheden = eenheden;
            this.omzetPerEenheid = omzetPerEenheid;
            this.omschrijving = omschrijving;
        }
        public static void leesRegels(string bron)
        {
            string[] regels = Lees.Regels(bron);
            foreach (string regel in regels)
            {
                if (regel.Trim() != "")
                {
                    string[] data = regel.Split(';');
                    string afdkod = data[0];
                    string regelNr = data[1];
                    string omzet = data[2];
                    string uren = data[3];
                    string manuurPrest = data[4];
                    string loonkost = data[5];
                    string loonkostPerOmzet = data[6];
                    string eenheden = data[7];
                    string omzetPerEenheid = data[8];
                    string omschrijving = "";
                    if (data.Count() > 9)
                    {
                        omschrijving = data[9];
                    }
                    lijst.Add(new Normtab(afdkod, regelNr, omzet, uren, manuurPrest, loonkost, loonkostPerOmzet, eenheden, omzetPerEenheid, omschrijving));
                }
            }

        }
       
        public static void maakDbImport(string doel)
        {
            string dbImport = genereerDbImportRecords();
            // create a writer and open the file
            TextWriter tw = new StreamWriter(doel);

            // write a line of text to the file
            tw.WriteLine(dbImport);

            // close the stream
            tw.Close();
        }
        static string genereerDbImportRecords()
        {
            string dbImport = "";
            foreach (Normtab regel in lijst)
            {
                try
                {
                    dbImport += "normtab~recnr=" + regel.afdkod + "~recnr~" + regel.afdkod + '\n';
                    dbImport += "normtab~recnr=" + regel.afdkod + "~normtabkod~" + regel.afdkod + '\n';
                    if (regel.omzetPerEenheid.Trim().Length > 0)
                        dbImport += "normtab~recnr=" + regel.afdkod + "~norm[" + regel.regelNr + "][6]~" + regel.omzetPerEenheid + '\n';
                    if (regel.eenheden.Trim().Length > 0)
                        dbImport += "normtab~recnr=" + regel.afdkod + "~norm[" + regel.regelNr + "][5]~" + regel.eenheden + '\n';
                    if (regel.loonkostPerOmzet.Trim().Length > 0)
                        dbImport += "normtab~recnr=" + regel.afdkod + "~norm[" + regel.regelNr + "][4]~" + regel.loonkostPerOmzet + '\n';
                    if (regel.loonkost.Trim().Length > 0)
                        dbImport += "normtab~recnr=" + regel.afdkod + "~norm[" + regel.regelNr + "][3]~" + regel.loonkost + '\n';
                    if (regel.manuurPrest.Trim().Length > 0)
                        dbImport += "normtab~recnr=" + regel.afdkod + "~norm[" + regel.regelNr + "][2]~" + regel.manuurPrest + '\n';
                    if (regel.uren.Trim().Length > 0)
                    {
                        if (Program.timeFormat.Length > 0)
                        {

                            regel.uren = (Convert.ToDouble(regel.uren) * 60).ToString("0000");
                        }
                        dbImport += "normtab~recnr=" + regel.afdkod + "~norm[" + regel.regelNr + "][1]~" + regel.uren + '\n';
                    }
                    if (regel.omzet.Trim().Length > 0)
                        dbImport += "normtab~recnr=" + regel.afdkod + "~norm[" + regel.regelNr + "][0]~" + regel.omzet + '\n';
                    dbImport += "normtab~recnr=" + regel.afdkod + "~omschr~" + regel.omschrijving + '\n';
                    dbImport += "normtab~recnr=" + regel.afdkod + "~activ~1\nnormtab~recnr=" + regel.afdkod + "~filler~\nnormtab~recnr=" + regel.afdkod + "~lckfld~\n";
                }
                catch (Exception ex)
                {
                }
            }
            return dbImport;
        }
    }
    
    /// <summary>
    /// lees importfiles
    /// </summary>
    class Lees
    {

        public static string[] Regels(string bron)
        {
            string[] regels = null;

            //open bestand
            try
            {
                TextReader tr = null;
                tr = new StreamReader(bron);
                string data = tr.ReadToEnd();
                regels = data.Split('\n');
            }
            catch (IOException ex)
            {

            }

            return regels;
        }
    }

    class Wksplit
    {
        public static List<Wksplit> lijst = new List<Wksplit>();
        string afdkod, regelNr,verdelingType, omschr, dagNr,type, van,tot,aantalVast,aantalVar;
        string maa, din, woe, don, vri, zat, zon;
        public Wksplit(string afdkod, string regelNr, string verdelingType, string omschr, string dagNr, string type, string van, string tot, string aantalVast, string aantalVar)
        {
            this.afdkod = afdkod;
            this.regelNr = regelNr;
            this.verdelingType = verdelingType;
            this.omschr = omschr;
            this.dagNr = dagNr;
            this.type = type;
            this.van = van;
            this.tot = tot;
            this.aantalVast = aantalVast;
            this.aantalVar = aantalVar;
        }
        public Wksplit(string afdkod, string regelNr, string verdelingType, string omschr, string maa, string din, string woe, string don, string vri, string zat, string zon)
        {
            this.afdkod = afdkod;
            this.regelNr = regelNr;
            this.verdelingType = verdelingType;
            this.omschr = omschr;
            this.maa = maa;
            this.din = din;
            this.woe = woe;
            this.don = don;
            this.vri = vri;
            this.zat = zat;
            this.zon = zon;
        }
        
        public static void leesRegels(string bron)
        {
            string[] regels = Lees.Regels(bron);
            foreach (string regel in regels)
            {
                try
                {
                    string[] data = regel.Split(';');
                    string regelNr = data[0];
                    string verdelingType = data[1];
                    string afdkod = data[2];
                    string omschr = data[3];

                    //per uur
                    if (Convert.ToInt32(verdelingType) > 3)
                    {
                        string dagNr = data[4];
                        string type = data[5];
                        string van = data[6];
                        if (Program.timeFormat.Length > 0)
                        {
                            van = ((Convert.ToInt32(data[6].Split(':')[0]) * 60)+Convert.ToInt32(data[6].Split(':')[1])).ToString("0000");
                        }
                        string tot = data[7];
                        if (Program.timeFormat.Length > 0)
                        {
                            tot = ((Convert.ToInt32(data[7].Split(':')[0]) * 60) + Convert.ToInt32(data[7].Split(':')[1])).ToString("0000");
                        }
                        string aantalVast = data[8];
                        string aantalVar = data[9];
                        lijst.Add(new Wksplit(afdkod, regelNr, verdelingType, omschr, dagNr, type, van, tot, aantalVast, aantalVar));
                    }
                    //per dag
                    else
                    {
                        string maa = data[4];
                        string din = data[5];
                        string woe = data[6];
                        string don = data[7];
                        string vri = data[8];
                        string zat = data[9];
                        string zon = data[10];
                        lijst.Add(new Wksplit(afdkod, regelNr, verdelingType, omschr, maa, din, woe, don, vri, zat, zon));
                    }
                }
                catch (Exception ex)
                {
                }
            }

        }
        public static void maakDbImport(string doel,string moni)
        {

            RegistryKey OurKey = Registry.LocalMachine;
            string sbPath = OurKey.OpenSubKey("SOFTWARE", true).OpenSubKey("OrgaTime").GetValue("tcbase").ToString()+"\\";
            List<String> del = new List<string>();
            del.Add(@"DEL "+sbPath+@"vst\splt*.bes");
            del.Add(@"DEL "+sbPath+@"vst\task*.bes");
            del.Add(@"type NUL > "+sbPath+@"vst\task.default.bes");
            Proces.cmd(del);
            string a = string.Empty;
            int b = 1;
            TextWriter tw2 = new StreamWriter(sbPath + "log/gencnv2.log");
            tw2.WriteLine((b / lijst.Count() * 100).ToString() + "%");
            tw2.Close();
            if (moni != "-moni")
            {
                List<string> cmds = new List<string>();
                cmds.Add(sbPath + "bin\\tcmoni " + sbPath + "scr/tcmoni.scr " + sbPath + "log/gencnv2.log ");
                Proces.cmdNoWait(cmds);
            }
            foreach (Wksplit regel in lijst)
            {
                if (a != "import van afdeling " + regel.afdkod)
                {
                    a = "import van afdeling " + regel.afdkod;
                    Console.WriteLine(a);
                    
                }
                
                tw2 = new StreamWriter(sbPath + "log/gencnv2.log");
                tw2.WriteLine("Verdeelsleutel " + b + " van " + lijst.Count);
                tw2.WriteLine((Convert.ToDecimal(b) / Convert.ToDecimal(lijst.Count()) * 100).ToString("0.##") + "%");
                for (int i = 1; i <= (Convert.ToDecimal(b) / Convert.ToDecimal(lijst.Count()) * 100); i++)
                {
                    tw2.Write("|");
                }
                for (int i = 100; i >= (Convert.ToDecimal(b) / Convert.ToDecimal(lijst.Count()) * 100); i--)
                {
                    tw2.Write(".");
                }
                tw2.Close();
                Console.WriteLine("Verdeelsleutel " + b + " van " + lijst.Count);
                b++;
                try
                {
                    File.Copy(sbPath + @"vst\wksplit.default.bes", sbPath + @"vst\wksplit.bes", true);
                }
                catch
                {
                }
                try
                {
                    File.Copy(sbPath + @"vst\splt" + regel.afdkod + @".bes", sbPath + @"vst\wksplit.bes", true);
                }
                catch
                {
                }
                try
                {
                    File.Copy(sbPath + @"vst\task.default.bes", sbPath + @"vst\task.bes", true);
                }
                catch
                {
                }
                try
                {
                    File.Copy(sbPath + @"vst\task" + regel.afdkod + @".bes", sbPath + @"vst\task.bes", true);
                }
                catch
                {
                }
                //import.Add(@"echo B | xcopy "+sbPath+@"vst\wksplit.default.bes "+sbPath+@"vst\wksplit.bes /c /R /-Y");
                //import.Add(@"echo B | xcopy "+sbPath+@"vst\splt" + regel.afdkod + @".bes "+sbPath+@"vst\wksplit.bes /c /R /-Y");
                //import.Add(@"echo B | xcopy "+sbPath+@"vst\task.default.bes "+sbPath+@"vst\task.bes /c /R /-Y");
                //import.Add(@"echo B | xcopy "+sbPath+@"vst\task" + regel.afdkod + @".bes "+sbPath+@"vst\task.bes /c /R /-Y");
                //import.Add(@"echo J | xcopy "+sbPath+@"vst\wksplit.default.bes "+sbPath+@"vst\wksplit.bes /c /R /-Y");
                //import.Add(@"echo J | xcopy "+sbPath+@"vst\splt" + regel.afdkod + @".bes "+sbPath+@"vst\wksplit.bes /c /R /-Y");
                //import.Add(@"echo J | xcopy "+sbPath+@"vst\task.default.bes "+sbPath+@"vst\task.bes /c /R /-Y");
                //import.Add(@"echo J | xcopy "+sbPath+@"vst\task" + regel.afdkod + @".bes "+sbPath+@"vst\task.bes /c /R /-Y");
                //Proces.cmd(import);
                string dbImport = "";
                //per uur
                if (Convert.ToInt32(regel.verdelingType) > 3)
                {
                    dbImport += "task~recnr=" + regel.regelNr + "~recnr~" + regel.regelNr + "\n";
                    dbImport += "task~recnr=" + regel.regelNr + "~omschr~" + regel.omschr + "\n";
                    dbImport += "task~recnr=" + regel.regelNr + "~wksplit~" + regel.verdelingType + "\n";
                    dbImport += "task~recnr=" + regel.regelNr + "~day~" + regel.dagNr + "\n";
                    dbImport += "task~recnr=" + regel.regelNr + "~van_tim~" + regel.van + "\n";
                    dbImport += "task~recnr=" + regel.regelNr + "~time~" + regel.tot + "\n";
                    dbImport += "task~recnr=" + regel.regelNr + "~type~" + regel.type + "\n";
                    if (regel.aantalVar.Trim().Length > 0)
                    {
                        dbImport += "task~recnr=" + regel.regelNr + "~value~" + regel.aantalVar + "\n";
                    }
                    else if (regel.aantalVast.Trim().Length > 0)
                    {
                        dbImport += "task~recnr=" + regel.regelNr + "~value~" + regel.aantalVast + "\n";
                    }
                    else
                    {
                        dbImport = "";
                    }
                }
                //per dag
                else
                {
                    dbImport += "wksplit~recnr=" + regel.verdelingType + "~recnr~" + regel.verdelingType + "\n";
                    dbImport += "wksplit~recnr=" + regel.verdelingType + "~proc[6]~" + regel.zon + "\n";
                    dbImport += "wksplit~recnr=" + regel.verdelingType + "~proc[5]~" + regel.zat + "\n";
                    dbImport += "wksplit~recnr=" + regel.verdelingType + "~proc[4]~" + regel.vri + "\n";
                    dbImport += "wksplit~recnr=" + regel.verdelingType + "~proc[3]~" + regel.don + "\n";
                    dbImport += "wksplit~recnr=" + regel.verdelingType + "~proc[2]~" + regel.woe + "\n";
                    dbImport += "wksplit~recnr=" + regel.verdelingType + "~proc[1]~" + regel.din + "\n";
                    dbImport += "wksplit~recnr=" + regel.verdelingType + "~proc[0]~" + regel.maa + "\n";
                }
                // create a writer and open the file
                TextWriter tw = new StreamWriter(doel);
                // write a line of text to the file
                tw.WriteLine(dbImport);

                // close the stream
                tw.Close();

                List<string> import = new List<string>();
                import.Add(@""+sbPath+@"bin\dbimport -d~ " + doel);
                Proces.cmd(import);
                try
                {
                    File.Copy(sbPath + @"vst\wksplit.bes", sbPath + @"vst\splt" + regel.afdkod + @".bes", true);
                }
                catch
                {
                }
                try
                {
                    File.Copy(sbPath + @"vst\task.bes", sbPath + @"vst\task" + regel.afdkod + @".bes", true);
                }
                catch
                {
                }
                //import.Add(@"echo B | xcopy "+sbPath+@"vst\wksplit.bes "+sbPath+@"vst\splt" + regel.afdkod + @".bes /c /R /-Y");
                //import.Add(@"echo B | xcopy "+sbPath+@"vst\task.bes "+sbPath+@"vst\task" + regel.afdkod + @".bes /c /R /-Y");
                //import.Add(@"echo J | xcopy "+sbPath+@"vst\wksplit.bes "+sbPath+@"vst\splt" + regel.afdkod + @".bes /c /R /-Y");
                //import.Add(@"echo J | xcopy "+sbPath+@"vst\task.bes "+sbPath+@"vst\task" + regel.afdkod + @".bes /c /R /-Y");
                
                
            }
            List<string> zf40 = new List<string>();
            zf40.Add("zf40_100.exe");
            Proces.cmd(zf40);
            

            
        }

        /*
         * */
    }
}
